房屋租赁管理系统，原型。<br/>
管理后台系统，原型。<br/>
企业管理系统，原型。<br/>
采用PHP编程语言开发。<br/>
采用ThinkPHP 3.2.3框架开发。<br/>
集成了：<br/>
1、采用EZDML做数据库设计。<br/>
2、RBAC权限系统。<br/>
3、多图上传。<br/>
4、单图上传。<br/>
5、集成高德地图。（附加案例）<br/>
6、集成jQuery EasyUI v1.5.1框架。（附加案例）<br/>
7、集成Bootstrap v3.3.7框架。（附加案例）<br/>
8、集成kindeditor文本编辑器。（潜入多图、单图、附件上传功能）<br/>
9、集成jQuery JSON转换框架。<br/>
10、集成jQuery Cookie框架。<br/>
11、修复jQuery ajaxfileupload框架上传的一些兼容性。<br/>
等。<br/>
持续开发中。<br/>
qq群：414045018。<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112132_21fa13ac_87555.png "20171027101437.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112144_0e204841_87555.png "20171027101454.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112155_14b5c1ef_87555.png "20171027101516.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112202_f66c69fd_87555.png "20171027101535.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112209_3379a1db_87555.png "20171027101552.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112246_c4297669_87555.png "20171027103308.png")<br/>
![输入图片说明](https://gitee.com/uploads/images/2017/1030/112252_95d9b446_87555.png "20171027103323.png")<br/>