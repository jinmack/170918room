
(function($){

	// 图片
	product = {

		//上传图片并显示 - 单图
		upPic:function(el){

			var id = $(el).attr('id');
			$.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajaxFileUpload({
				url:'/ServicePic/picSave?dir=image',
				type:'post',
				secureuri:true,
				fileElementId:id,
				dataType:'text/html',
				success:function(data,status){

					var data = eval('('+data+')');
					$.messager.progress('close');
					if( data.info == 'success' ){

						$('#'+id+'Child').html("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block;float:left;'>"
												+"<img class='picImg' width='100' height='100' src='"+data.picAddress+"' savename='"+data.savename+"'/>"
												+"<button onclick='product.removePic(this)' style='position: absolute;right:0px;bottom:0px'>删除</button>"
												+"</div>");

						$("#"+id).val('');

					} else {
						
						$.messager.alert('提示信息', data.info);
					}
				}
			})
		},

		//删除图片 - 单图
		removePic:function(el){

			$(el).parent().remove();
		},




		//上传附件并显示 - 单附件
		upEnclosure:function(el){

			var id = $(el).attr('id');
			$.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajaxFileUpload({
				url:'/ServicePic/picSave?dir=file',
				type:'post',
				secureuri:true,
				fileElementId:id,
				dataType:'text/html',
				success:function(data,status){

					var data = eval('('+data+')');
					$.messager.progress('close');
					if( data.info == 'success' ){

						$('#'+id+'Child').html("<div class='picShowWrap' style='position:relative;display:inline-block;float:left;'>"
												+"<a class='picImg' src='"+data.picAddress+"' savename='"+data.savename+"' href='"+data.picAddress+"' target='_blank'>"+data.savename+"</a>"
												+"<button onclick='product.removeEnclosure(this)' style='position: absolute;right:0px;bottom:0px'>删除</button>"
												+"</div>");

						$("#"+id).val('');

					} else {
						
						$.messager.alert('提示信息', data.info);
					}
				}
			})
		},

		//删除附件 - 单附件
		removeEnclosure:function(el){

			$(el).parent().remove();
		},
		




		//上传图片并显示 - 多图
		upPicMany:function(el){

			var id = $(el).attr('id');

			$.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajaxFileUpload({
				url:'/ServicePic/picSave?dir=image',
				type:'post',
				secureuri:true,
				fileElementId:id,
				dataType:'text/html',
				success:function(data,status){

					var data = eval('('+data+')');
					$.messager.progress('close');
					if( data.info == 'success' ){

						$('#'+id+'Child').append("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block;float:left;'>"
												+"<img class='picImg' width='100' height='100' src='"+data.picAddress+"' savename='"+data.savename+"'/>"
												+"<button onclick='product.movePicMany(this)' style='position: absolute;left:0px;bottom:0px'>左移</button>"
												+"<button onclick='product.deletePicMany(this)' style='position: absolute;right:0px;bottom:0px'>删除</button>"
												+"</div>");

						$("#"+id).val('');

					} else {
						
						$.messager.alert('提示信息', data.info);
					}
				}
			})
		},

		//删除图片 - 多图
		deletePicMany:function(el){

			$(el).parent().remove();
		},

		//移动图片 - 多图
		movePicMany:function(el){
			var index = $(el).parent().index() ;
			if(index != 0){
				var str1 = $(el).parent().html();
				var str2 = $(el).parent().parent().find(".picShowWrap").eq(index-1).html();
				$(el).parent().parent().find(".picShowWrap").eq(index-1).html(str1);
				 $(el).parent().html(str2);
			}else{
				alert("已经是首位了！");
			}
		},

	}

	// 权限
	authority = {

		// 权限列表
		// select标签组
		list:function(id){

			$("#authority").empty();
			$.get("/adminAuthority/adminAuthorityList",
				function(data){
					if(data != null){
						var str = '<select>';
						str += '<option value="0">无</option>';
						for(var i = 0; i<data.rows.length; i++){
							if(data.rows[i].id == id){
								str += "<option value='"+data.rows[i].id+"' selected='selected' >"+data.rows[i].roleName+"</option>";
							}else{
								str += "<option value='"+data.rows[i].id+"'>"+data.rows[i].roleName+"</option>";
							}
						}
						str += '</select>';
						$("#authority").append(str);
					}
				}
			);
		},

	}

	// 分类
	classification = {

		// 产品分类
		listProductClassification:function(id){

			$("#classification").empty();
			$.get("/adminProductClassification/adminProductClassificationJsonSeleftTotal",
				function(data){
					if(data != null){
						var str = '<select>';
						// str += '<option value="0">无</option>';
						for(var i = 0; i<data.rows.length; i++){
							if(data.rows[i].id == id){
								str += "<option value='"+data.rows[i].id+"' selected='selected' >"+data.rows[i].productClassificationName+"</option>";
							}else{
								str += "<option value='"+data.rows[i].id+"'>"+data.rows[i].productClassificationName+"</option>";
							}
						}
						str += '</select>';
						$("#classification").append(str);
					}
				}
			);
		},

		// 新闻分类
		listNewsClassification:function(id){

			$("#classification").empty();
			$.get("/adminNewsClassification/adminNewsClassificationJsonSeleftTotal",
				function(data){
					if(data != null){
						var str = '<select>';
						// str += '<option value="0">无</option>';
						for(var i = 0; i<data.rows.length; i++){
							if(data.rows[i].id == id){
								str += "<option value='"+data.rows[i].id+"' selected='selected' >"+data.rows[i].newsClassificationName+"</option>";
							}else{
								str += "<option value='"+data.rows[i].id+"'>"+data.rows[i].newsClassificationName+"</option>";
							}
						}
						str += '</select>';
						$("#classification").append(str);
					}
				}
			);
		},

		// 附件分类
		listEnclosureClassification:function(id){

			$("#classification").empty();
			$.get("/adminEnclosureClassification/adminEnclosureClassificationJsonSeleftTotal",
				function(data){
					if(data != null){
						var str = '<select>';
						// str += '<option value="0">无</option>';
						for(var i = 0; i<data.rows.length; i++){
							if(data.rows[i].id == id){
								str += "<option value='"+data.rows[i].id+"' selected='selected' >"+data.rows[i].enclosureClassificationName+"</option>";
							}else{
								str += "<option value='"+data.rows[i].id+"'>"+data.rows[i].enclosureClassificationName+"</option>";
							}
						}
						str += '</select>';
						$("#classification").append(str);
					}
				}
			);
		},

	}

})(jQuery);
