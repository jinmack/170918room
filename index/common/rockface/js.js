﻿function setNav(i) {
    $(".head .nav ul li").eq(i).addClass("on");
}

function errorImg(img) {
    img.src = "images/onerror.jpg";
    img.onerror = null;
}

$(function () {
    $("#btnSumbit").click(function () {
        var currentVal = $("#KK_name").val();
        if (currentVal == "") {
            layer.msg("Please enter your name！", 1);
            $("#KK_name").focus();
            return false;
        }
        if (currentVal.length > 28) {
            layer.msg("Please enter the correct name！", 1);
            $("#KK_name").focus();
            return false;
        }
        var currentVal = $("#KK_Email").val();
        if (currentVal == "") {
            layer.msg("Please enter your email！", 1);
            $("#KK_Email").focus();
            return false;
        }
        var reg = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
        if (!reg.test(currentVal)) {
            layer.msg("Please enter the correct email！", 1);
            $("#KK_Email").focus();
            return false;
        }
        var currentVal = $("#KK_phone").val();
        if (currentVal == "") {
            layer.msg("Please enter your phone！", 1);
            $("#KK_phone").focus();
            return false;
        }
        //if (!(/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/.test(currentVal))) {
        //    layer.msg("Please enter the correct phone！", 1);
        //    $("#KK_phone").focus();
        //    return false;
        //}
        if (!(/^[0-9]*$/.test(currentVal))) {
            layer.msg("Please enter the correct phone！", 1);
            $("#KK_phone").focus();
            return false;
        }
        currentVal = $("#KK_content").val();
        if (currentVal == "") {
            layer.msg("Please enter the message content！", 1);
            $("#KK_content").focus();
            return false;
        }
        var flage1 = false;
        currentVal = $("#txtCode").val();
        if (currentVal == "") {
            layer.msg("Please enter the verification code！", 1);
            $("#txtCode").focus();
            return false;
        }
        jQuery.ajax({
            type: 'POST',
            url: '/ajax/CheckREG.aspx?action=checkform&type=code',
            data: 'values=' + $("#txtCode").val(),
            async: false,
            success: function (msg) {
                if (msg == "0") {
                    flage1 = false;
                    layer.msg("Please enter the correct verification code！", 1);
                }
                else {
                    flage1 = true;
                }
            },
            error: function () {
                alert("error");
                flage1 = false;
            }
        });
        return flage1;
    });

    $("#aFloatTools_Show").click(function () {
        $('#divFloatToolsView').animate({ width: 'show', opacity: 'show' }, 100, function () { $('#divFloatToolsView').show(); });
        $('#aFloatTools_Show').hide();
        $('#aFloatTools_Hide').show();
    });
    $("#aFloatTools_Hide").click(function () {
        $('#divFloatToolsView').animate({ width: 'hide', opacity: 'hide' }, 100, function () { $('#divFloatToolsView').hide(); });
        $('#aFloatTools_Show').show();
        $('#aFloatTools_Hide').hide();
    });

    var flag = 0;
    $('#rightArrow').on("click", function () {
        if (flag == 1) {
            $("#floatDivBoxs").animate({ right: '-175px' }, 300);
            $(this).animate({ right: '-5px' }, 300);
            $(this).css('background-position', '-50px 0');
            flag = 0;
        } else {
            $("#floatDivBoxs").animate({ right: '0' }, 300);
            $(this).animate({ right: '170px' }, 300);
            $(this).css('background-position', '0px 0');
            flag = 1;
        }
    });

    //初始隐藏
    $("#side_content").animate({ width: '0px' }, "fast");
    $("#show_btn").animate({ width: '25px' }, "fast");

    $("#show_btn").click(
    function () {
        $(this).animate({ width: '0px' }, "fast");
        $("#side_content").stop(true, true).delay(200).animate({ width: '154px' }, "fast");
    });

    $("#close_btn").click(
    function () {
        $("#side_content").animate({ width: '0px' }, "fast");
        $("#show_btn").stop(true, true).delay(300).animate({ width: '25px' }, "fast");
    });

});