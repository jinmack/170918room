<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用入口文件

// 允许跨域
header('Access-Control-Allow-Origin: *');
// 编码格式修正
header('Content-Type: text/html; charset=utf-8');
// 设定用于一个脚本中所有日期时间函数的默认时区
date_default_timezone_set('Asia/Shanghai');
// 取消PHP的内存限制
// ini_set("memory_limit","1024M");



// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG', true);

// 绑定访问Index模块
define('BIND_MODULE', 'Index');

// 目录安全文件
define('DIR_SECURE_FILENAME', 'index.html');

// 定义应用目录
define('APP_PATH', '../Application/');

// 引入ThinkPHP入口文件
require '../ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单
