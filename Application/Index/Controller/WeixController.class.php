<?php
namespace Index\Controller;
use Think\Controller;
class WeixController extends BaseController {

    // 用于主页显示
	public function index(){

        try {

            // dump($_GET);
            // dump($_POST);
            // dump($_COOKIE);
            // dump($_SESSION);
            // dump($_SERVER["REMOTE_ADDR"]);

            // echo 'index/weix/weix';
            $this->display('weix/common/head');
            $this->display('weix/weix/weix');
            $this->display('weix/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
	}

}
