<?php
namespace Index\Controller;
use Think\Controller;
class AdminMerchantController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminMerchant(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminMerchant/query');
            $this->display('admin/adminMerchant/adminMerchant');
            $this->display('admin/adminMerchant/add');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
