<?php
namespace Index\Controller;
use Think\Controller;
class DaoHouseController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();
    }

    public function daoHouse(){

    }

    public function house(){

    }

    // 数据查询
    public function houseJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $cellName = I('post.cellName',null);
            $buildingName = I('post.buildingName',null);
            $name = I('post.name',null);
            $roomNumber = I('post.roomNumber',null);

            if(!empty($cellName)){
                $cellName = " and s.cellName LIKE '%".$cellName."%' ";
            }
            if(!empty($buildingName)){
                $buildingName = " and b.buildingName LIKE '%".$buildingName."%' ";
            }
            if(!empty($name)){
                $name = " and h.name LIKE '%".$name."%' ";
            }
            if(!empty($roomNumber)){
                $roomNumber = " and h.roomNumber LIKE '%".$roomNumber."%' ";
            }
            $whereStr = $cellName . $buildingName . $name . $roomNumber;

            $list = M("house as h")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" h.status != -100 AND b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                h.id,h.name,h.roomNumber,h.rent,h.face,h.floor,h.mode,h.apartmentLayout,h.renovation,h.orientation,h.supportingFacilities,h.describe,h.picture,h.buildingID,h.createTime,
                b.buildingName,
                s.cellName
                ")
            ->order(" h.createTime desc,h.id ")
            ->limit(($page-1)*$rows,$rows)
            ->select();

            $count = M("house as h")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" h.status != -100 AND b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                h.id
                ")
            ->order(" h.createTime desc,h.id ")
            ->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据添加
    public function houseAdd(){

        try {

            $buildingID = I('post.buildingID',null);
            $name = I('post.name',null);
            $roomNumber = I('post.roomNumber',null);
            $rent = I('post.rent',null);
            $face = I('post.face',null);
            $floor = I('post.floor',null);
            $mode = I('post.mode',null);
            $apartmentLayout = I('post.apartmentLayout',null);
            $renovation = I('post.renovation',null);
            $orientation = I('post.orientation',null);
            $supportingFacilities = I('post.supportingFacilities',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);

            if(empty($buildingID)) { throw new \Exception( '请输入小区/栋！' ); }
            if(empty($roomNumber)) { throw new \Exception( '请输入房产房号！' ); }

            $roomNumberIf = M("building as b")
            ->join(" small_area as s on b.`smallAreaID` = s.`id` ")
            ->join(" house as h on h.`buildingID` = b.`id` ")
            ->where(" b.status != -100 AND s.status != -100 AND h.status != -100 AND b.id = '$buildingID' AND h.roomNumber = '$roomNumber' ")
            ->field("
                s.id,s.cellName,b.buildingName,h.roomNumber
                ")
            ->order(" b.createTime desc,b.id ")
            ->find();
            if($roomNumberIf['roomNumber']){
                throw new \Exception( '请填入其他房号！' );
            }

            $house = M('house');
            $data['name'] = $name;
            $data['roomNumber'] = $roomNumber;
            $data['rent'] = $rent;
            $data['face'] = $face;
            $data['floor'] = $floor;
            $data['mode'] = $mode;
            $data['apartmentLayout'] = htmlspecialchars_decode($apartmentLayout);
            $data['renovation'] = $renovation;
            $data['orientation'] = $orientation;
            $data['supportingFacilities'] = htmlspecialchars_decode($supportingFacilities);
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);
            $data['buildingID'] = $buildingID;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $house->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function houseSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $house = M('house');
            $data['status'] = $num;
            $house->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // ID查询
    public function houseIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $list = M("house as h")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" h.id=$id ")
            ->field("
                h.id,h.name,h.roomNumber,h.rent,h.face,h.floor,h.mode,h.apartmentLayout,h.renovation,h.orientation,h.supportingFacilities,h.describe,h.picture,h.buildingID,
                b.buildingName,
                s.cellName
                ")
            ->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据修改
    public function houseSaveData(){

        try {

            $buildingID = I('post.buildingID',null);
            $name = I('post.name',null);
            $roomNumber = I('post.roomNumber',null);
            $rent = I('post.rent',null);
            $face = I('post.face',null);
            $floor = I('post.floor',null);
            $mode = I('post.mode',null);
            $apartmentLayout = I('post.apartmentLayout',null);
            $renovation = I('post.renovation',null);
            $orientation = I('post.orientation',null);
            $supportingFacilities = I('post.supportingFacilities',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);
            $id = I('post.id',null);

            if(empty($buildingID)) { throw new \Exception( '请输入小区/栋！' ); }
            if(empty($roomNumber)) { throw new \Exception( '请输入房产房号！' ); }

            $houseIf = M('house'); // 171021. 判断重复值，选择的方案，无法关系两张表问题，最后只能选择关系一张表问题
            $houseIf = $houseIf->where("id=$id AND status != -100")->find();
            if( $roomNumber != $houseIf['roomNumber'] ){ // 栋名称发生改变时执行
                $houseIf = M('house');
                $houseIf = $houseIf->where("buildingID=$buildingID AND roomNumber='$roomNumber' AND status != -100")->find();
                if( $roomNumber == $houseIf['roomNumber'] ){
                    throw new \Exception( '请填入其他房产房号！' );
                }
            }

            $house = M('house');
            $data['name'] = $name;
            $data['roomNumber'] = $roomNumber;
            $data['rent'] = $rent;
            $data['face'] = $face;
            $data['floor'] = $floor;
            $data['mode'] = $mode;
            $data['apartmentLayout'] = htmlspecialchars_decode($apartmentLayout);
            $data['renovation'] = $renovation;
            $data['orientation'] = $orientation;
            $data['supportingFacilities'] = htmlspecialchars_decode($supportingFacilities);
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);
            $data['buildingID'] = $buildingID;
            $house->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
