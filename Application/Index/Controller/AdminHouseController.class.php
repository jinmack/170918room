<?php
namespace Index\Controller;
use Think\Controller;
class AdminHouseController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminHouse(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminHouse/query');
            $this->display('admin/adminHouse/adminHouse');
            $this->display('admin/adminHouse/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 房产添加栋
    public function adminHouseAddBuilding(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminHouseAddBuilding/query');
            $this->display('admin/adminHouseAddBuilding/adminHouseAddBuilding');
            $this->display('admin/adminHouseAddBuilding/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
