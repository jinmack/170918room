<?php
namespace Index\Controller;
use Think\Controller;
class AdminSmallAreaController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminSmallArea(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminSmallArea/query');
            $this->display('admin/adminSmallArea/adminSmallArea');
            $this->display('admin/adminSmallArea/add');
            $this->display('admin/common/tail');
            
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
