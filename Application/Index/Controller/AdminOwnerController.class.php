<?php
namespace Index\Controller;
use Think\Controller;
class AdminOwnerController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminOwner(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminOwner/query');
            $this->display('admin/adminOwner/adminOwner');
            $this->display('admin/adminOwner/add');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
