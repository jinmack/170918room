<?php
namespace Index\Controller;
use Think\Controller;
class AdminOwnerFeeStandardController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminOwnerFeeStandard(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminOwnerFeeStandard/query');
            $this->display('admin/adminOwnerFeeStandard/adminOwnerFeeStandard');
            $this->display('admin/adminOwnerFeeStandard/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 用于添加房产弹框选择用
    public function adminOwnerFeeStandardAddHouse(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminOwnerFeeStandardAddHouse/query');
            $this->display('admin/adminOwnerFeeStandardAddHouse/adminOwnerFeeStandardAddHouse');
            $this->display('admin/adminOwnerFeeStandardAddHouse/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 用于添加业主弹框选择用
    public function adminOwnerFeeStandardAddOwner(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminOwnerFeeStandardAddOwner/query');
            $this->display('admin/adminOwnerFeeStandardAddOwner/adminOwnerFeeStandardAddOwner');
            $this->display('admin/adminOwnerFeeStandardAddOwner/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
