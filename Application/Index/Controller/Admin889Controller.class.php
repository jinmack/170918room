<?php
namespace Index\Controller;
use Think\Controller;
class Admin889Controller extends BaseController {

    public function index(){

    }

    public function admin889(){

    }

    // 后台主页面
    public function main(){

        try {

            // $aid = cookie('aid');
            // if($aid == null){
            //     header( "refresh:0;url=/manage/login" );
            //     exit();
            // }

            // $adminAuthority = session('adminAuthority');
            // $this->assign('adminAuthority', json_decode($adminAuthority));

            $this->display('admin/common/head');
            $this->display('admin/main/main');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
