<?php
namespace Index\Controller;
use Think\Controller;
class DaoUsersController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoUsers(){

    }

    public function users(){

    }

    // 数据查询
    public function usersJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $phone = I('post.phone',null);
            
            if(!empty($phone)){
                $phone = " and phone LIKE '%".$phone."%' ";
            }
            $whereStr = $phone;

            $users = M('users');
            $list = $users->where("status != -100 $whereStr")->order('createTime desc,id')->limit(($page-1)*$rows,$rows)->select();
            $count = $users->where("status != -100 $whereStr")->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据添加
    public function usersAdd(){

        try {

            $phone = I('post.phone',null);
            $password = I('post.password',null);
            $nickname = I('post.nickname',null);
            $fullName = I('post.fullName',null);
            $idCard = I('post.idCard',null);
            $headPortrait = I('post.headPortrait',null);
            $gender = I('post.gender',null);
            $eMail = I('post.eMail',null);
            $addressContacts = I('post.addressContacts',null);
            $mobilePhoneNumberOfAddress = I('post.mobilePhoneNumberOfAddress',null);
            $addressArea = I('post.addressArea',null);
            $addressDetailAddress = I('post.addressDetailAddress',null);
            $postalCodeOfAddress = I('post.postalCodeOfAddress',null);

            // if(empty($userName)) throw new \Exception( '请输入员工名称！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );

            $phoneIf = M('users');
            $phoneIf = $phoneIf->where("phone=$phone AND status != -100")->find();
            if( $phone == $phoneIf['phone'] ){
                throw new \Exception( '请输入其他手机号！' );
            }

            $users = M('users');
            $data['phone'] = $phone;
            $data['password'] = md5($password);
            $accounts = 'ro_' . date("ymdHis") . 'm' . rand(11111, 99999);
            $data['accounts'] = $accounts;
            $data['nickname'] = $nickname;
            $data['fullName'] = $fullName;
            $data['idCard'] = $idCard;
            $data['headPortrait'] = htmlspecialchars_decode($headPortrait);
            $data['gender'] = $gender;
            $data['eMail'] = $eMail;
            $data['addressContacts'] = $addressContacts;
            $data['mobilePhoneNumberOfAddress'] = $mobilePhoneNumberOfAddress;
            $data['addressArea'] = $addressArea;
            $data['addressDetailAddress'] = $addressDetailAddress;
            $data['postalCodeOfAddress'] = $postalCodeOfAddress;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $users->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function usersSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            // 删除用户表
            $users = M('users');
            $data['status'] = $num;
            $users->where("id=$id")->save($data);

            // 删除业主表
            $owner = M('owner');
            $data['status'] = $num;
            $owner->where("userID=$id")->save($data);

            // 删除租户表
            $tenant = M('tenant');
            $data['status'] = $num;
            $tenant->where("userID=$id")->save($data);

            // 删除商户表
            $merchant = M('merchant');
            $data['status'] = $num;
            $merchant->where("userID=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // ID查询
    public function usersIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $users = M('users');
            $list = $users->where("id=$id")->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据修改
    public function usersSaveData(){

        try {

            $phone = I('post.phone',null);
            $password = I('post.password',null);
            $nickname = I('post.nickname',null);
            $fullName = I('post.fullName',null);
            $idCard = I('post.idCard',null);
            $headPortrait = I('post.headPortrait',null);
            $gender = I('post.gender',null);
            $eMail = I('post.eMail',null);
            $addressContacts = I('post.addressContacts',null);
            $mobilePhoneNumberOfAddress = I('post.mobilePhoneNumberOfAddress',null);
            $addressArea = I('post.addressArea',null);
            $addressDetailAddress = I('post.addressDetailAddress',null);
            $postalCodeOfAddress = I('post.postalCodeOfAddress',null);
            $id = I('post.id',null);

            // if(empty($id)) throw new \Exception( '数据错误！' );
            // if(empty($userName)) throw new \Exception( '请输入用户名称！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );
            // if($adminAuthorityId == null) throw new \Exception( '请输入角色！' );

            $phoneIf = M('users');
            $phoneIf = $phoneIf->where("id=$id AND status != -100")->find();
            if( $phone != $phoneIf['phone'] ){ // 手机发生改变时执行
                $phoneIf = M('users');
                $phoneIf = $phoneIf->where("phone=$phone AND status != -100")->find();
                if( $phone == $phoneIf['phone'] ){
                    throw new \Exception( '请输入其他手机号！' );
                }
            }

            $UsersPassword = M('users');
            $UsersPassword = $UsersPassword->where("id=$id")->find();
            if($password == $UsersPassword['password']){
	            $data['password'] = $password;
            } else {
	            $data['password'] = md5($password);
            }

            $users = M('users');
            $data['phone'] = $phone;
            $data['nickname'] = $nickname;
            $data['fullName'] = $fullName;
            $data['idCard'] = $idCard;
            $data['headPortrait'] = htmlspecialchars_decode($headPortrait);
            $data['gender'] = $gender;
            $data['eMail'] = $eMail;
            $data['addressContacts'] = $addressContacts;
            $data['mobilePhoneNumberOfAddress'] = $mobilePhoneNumberOfAddress;
            $data['addressArea'] = $addressArea;
            $data['addressDetailAddress'] = $addressDetailAddress;
            $data['postalCodeOfAddress'] = $postalCodeOfAddress;
            $users->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
