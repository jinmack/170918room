<?php
namespace Index\Controller;
use Think\Controller;
class DaoOwnersController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoOwners(){

    }

    public function owners(){

    }

    // 数据查询
    public function ownersJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $phone = I('post.phone',null);
            
            if(!empty($phone)){
                $phone = " and u.phone LIKE '%".$phone."%' ";
            }
            $whereStr = $phone;

            $users = M('users')->table("users as u")->join("owner as o")->field("u.phone,u.accounts,u.nickname,u.fullName,u.headPortrait,u.eMail,o.id,o.createTime");
            $list = $users->where("u.status != -100 AND o.status != -100 $whereStr AND u.`id` = o.`userID` ")->order('o.createTime desc,o.id')->limit(($page-1)*$rows,$rows)->select();
            $users = M('users')->table("users as u")->join("owner as o")->field("u.id");
            $count = $users->where("u.status != -100 AND o.status != -100 $whereStr AND u.`id` = o.`userID` ")->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function ownersSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $owner = M('owner');
            $data['status'] = $num;
            $owner->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 添加用户到业主里
    public function usersAddOwners(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $ownerIf = M('owner');
            $ownerIf = $ownerIf->where("userID=$id AND status != -100")->find();
            if( $id == $ownerIf['userID'] ){
                throw new \Exception( '请输入其他业主！' );
            }

            $owner = M('owner');
            $data['userID'] = $id;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $owner->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }


}
