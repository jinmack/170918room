<?php
namespace Index\Controller;
use Think\Controller;
class AdminUserController extends BaseController {

	public function index(){

	}

    // 员工页面显示
    public function adminUser(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminUser/query');
            $this->display('admin/adminUser/adminUser');
            $this->display('admin/adminUser/add');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 员工数据查询
    public function adminUserJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $userName = I('post.userName',null);
            $email = I('post.email',null);
            $phone = I('post.phone',null);
            
            if(!empty($userName)){
                $userName = " and userName LIKE '%".$userName."%' ";
            }
            if(!empty($email)){
                $email = " and email LIKE '%".$email."%' ";
            }
            if(!empty($phone)){
                $phone = " and phone LIKE '%".$phone."%' ";
            }
            $whereStr = $userName . $email . $phone;

            $adminUser = M('admin_user');
            $listAdminUser = $adminUser->where("status != -100 $whereStr")->order('createTime desc,id')->limit(($page-1)*$rows,$rows)->select();
            $count = $adminUser->where("status != -100 $whereStr")->count();

            $adminAuthority = M('admin_authority');
            $listAdminAuthority = $adminAuthority->where("status != -100")->order('createTime desc,id')->select();
            
            for ($i=0; $i < count($listAdminUser); $i++) { 
                for ($j=0; $j < count($listAdminAuthority); $j++) { 
                    if($listAdminUser[$i]['adminAuthorityId'] == $listAdminAuthority[$j]['id']){
                        $listAdminUser[$i]['adminAuthority'] = $listAdminAuthority[$j];
                    }
                }
            }

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $listAdminUser;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工数据添加
    public function adminUserAdd(){

        try {

            $userName = I('post.userName',null);
            $password = I('post.password',null);
            $email = I('post.email',null);
            $phone = I('post.phone',null);
            $adminAuthorityId = I('post.adminAuthorityId',null);

            if(empty($userName)) { throw new \Exception( '请输入员工名称！' ); }
            if(empty($password)) { throw new \Exception( '请输入密码！' ); }
            if(empty($email)) { throw new \Exception( '请输入邮箱！' ); }
            if(empty($phone)) { throw new \Exception( '请输入电话！' ); }
            if($adminAuthorityId == null) { throw new \Exception( '请输入角色！' ); }

            $adminUser = M('admin_user');
            $data['userName'] = $userName;
            $data['password'] = md5($password);
            $data['email'] = $email;
            $data['phone'] = $phone;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $data['adminAuthorityId'] = $adminAuthorityId;
            $adminUser->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工状态修改
    public function adminUserSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $adminUser = M('admin_user');
            $data['status'] = $num;
            $adminUser->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工id查询
    public function adminUserIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $adminUser = M('admin_user');
            $list = $adminUser->where("id=$id")->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工数据修改
    public function adminUserSaveData(){

        try {

            $userName = I('post.userName',null);
            $password = I('post.password',null);
            $email = I('post.email',null);
            $phone = I('post.phone',null);
            $id = I('post.id',null);
            $adminAuthorityId = I('post.adminAuthorityId',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($userName)) { throw new \Exception( '请输入用户名称！' ); }
            if(empty($password)) { throw new \Exception( '请输入密码！' ); }
            if(empty($email)) { throw new \Exception( '请输入邮箱！' ); }
            if(empty($phone)) { throw new \Exception( '请输入电话！' ); }
            if($adminAuthorityId == null) { throw new \Exception( '请输入角色！' ); }

            $loginAdminUser = M('admin_user');
            $loginAdminUser = $loginAdminUser->where("id=$id")->find();

            if($password == $loginAdminUser['password']){
                $adminUser = M('admin_user');
                $data['userName'] = $userName;
                $data['email'] = $email;
                $data['phone'] = $phone;
                $data['adminAuthorityId'] = $adminAuthorityId;
                $adminUser->where("id=$id")->save($data);
            } else {
                $adminUser = M('admin_user');
                $data['userName'] = $userName;
                $data['password'] = md5($password);
                $data['email'] = $email;
                $data['phone'] = $phone;
                $data['adminAuthorityId'] = $adminAuthorityId;
                $adminUser->where("id=$id")->save($data);
            }

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }
}
