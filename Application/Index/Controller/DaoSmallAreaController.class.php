<?php
namespace Index\Controller;
use Think\Controller;
class DaoSmallAreaController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoSmall(){

    }

    public function smallArea(){

    }

    // 数据查询
    public function smallAreaJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $code = I('post.code',null);
            $cellName = I('post.cellName',null);

            if(!empty($code)){
                $code = " and code LIKE '%".$code."%' ";
            }
            if(!empty($cellName)){
                $cellName = " and cellName LIKE '%".$cellName."%' ";
            }
            $whereStr = $code . $cellName;

            $smallArea = M('small_area');
            $list = $smallArea->where("status != -100 $whereStr")->order('createTime desc,id')->limit(($page-1)*$rows,$rows)->select();
            $count = $smallArea->where("status != -100 $whereStr")->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据添加
    public function smallAreaAdd(){

        try {

            $cellName = I('post.cellName',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);

            $province = I('post.province',null);
            $city = I('post.city',null);
            $district = I('post.district',null);
            $address = I('post.address',null);
            $position_y = I('post.position_y',null);
            $position_x = I('post.position_x',null);
            $position_place = I('post.position_place',null);
            $link = I('post.link',null);

            // if(empty($userName)) throw new \Exception( '请输入员工名称！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );

            $cellNameIf = M('small_area');
            $cellNameIf = $cellNameIf->where("cellName='$cellName' AND status != -100")->find();
            if( $cellName == $cellNameIf['cellName'] ){
                throw new \Exception( '请输入其他小区名称！' );
            }

            $smallArea = M('small_area');
            $code = 'sm_' . date("ymdHis") . 'a' . rand(11111, 99999);
            $data['code'] = $code;
            $data['cellName'] = $cellName;
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);

            $data['province'] = $province;
            $data['city'] = $city;
            $data['district'] = $district;
            $data['address'] = $address;
            $data['position_y'] = $position_y;
            $data['position_x'] = $position_x;
            $data['position_place'] = $position_place;
            $data['link'] = htmlspecialchars_decode($link);

            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $smallArea->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function smallAreaSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            // 删除小区
            $smallArea = M('small_area');
            $data['status'] = $num;
            $smallArea->where("id=$id")->save($data);

            // 删除栋
            $building = M('building');
            $data['status'] = $num;
            $building->where("smallAreaID=$id")->save($data);

            // 删除房源

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // ID查询
    public function smallAreaIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $smallArea = M('small_area');
            $list = $smallArea->where("id=$id")->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据修改
    public function smallAreaSaveData(){

        try {

            $cellName = I('post.cellName',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);
            $province = I('post.province',null);
            $city = I('post.city',null);
            $district = I('post.district',null);
            $address = I('post.address',null);
            $position_y = I('post.position_y',null);
            $position_x = I('post.position_x',null);
            $position_place = I('post.position_place',null);
            $link = I('post.link',null);
            $id = I('post.id',null);

            // if(empty($id)) throw new \Exception( '数据错误！' );
            // if(empty($userName)) throw new \Exception( '请输入用户名称！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );
            // if($adminAuthorityId == null) throw new \Exception( '请输入角色！' );

            $cellNameIf = M('small_area');
            $cellNameIf = $cellNameIf->where("id=$id AND status != -100")->find();
            if( $cellName != $cellNameIf['cellName'] ){
                $cellNameIf = M('small_area');
                $cellNameIf = $cellNameIf->where("cellName='$cellName' AND status != -100")->find();
                if( $cellName == $cellNameIf['cellName'] ){
                    throw new \Exception( '请输入其他小区名称！' );
                }
            }

            $smallArea = M('small_area');
            $data['cellName'] = $cellName;
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);
            $data['province'] = $province;
            $data['city'] = $city;
            $data['district'] = $district;
            $data['address'] = $address;
            $data['position_y'] = $position_y;
            $data['position_x'] = $position_x;
            $data['position_place'] = $position_place;
            $data['link'] = htmlspecialchars_decode($link);
            $data['updateTime'] = date("Y-m-d H:i:s");
            $smallArea->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
