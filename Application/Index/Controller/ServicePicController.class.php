<?php
namespace Index\Controller;
use Think\Controller;
class ServicePicController extends ServiceController {

    public function index(){

    }

    // 图片保存-单图
    public function picSave(){

        try {

            $dir = I('get.dir');

            $upload = new \Think\Upload(); //实例化上传类
            $upload->maxSize = 200000; //200kb设置附件上传大小
            $upload->subName = array('date', 'Ymd'); //子目录名称
            $upload->saveName = time().'_'.mt_rand(); //上传文件名称
            // 设置附件上传类型
            switch ($dir){
                case 'image':
                    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');
                    $upload->rootPath = 'attachment/image/'; //设置附件上传目录
                    break;
                case 'file':
                    $upload->exts      =     array('pptx','ppt','docx','doc','txt','dotx','xlsx','xls','jpg','png');
                    $upload->rootPath = 'attachment/file/'; //设置附件上传目录
                    break;
                case 'flash':
                    $upload->exts      =     array('');
                    $upload->rootPath = 'attachment/flash/'; //设置附件上传目录
                    break;
                case 'media':
                    $upload->exts      =     array('');
                    $upload->rootPath = 'attachment/media/'; //设置附件上传目录
                    break;
                default:
                    $info = $upload->upload();
                    if(!$info) { throw new \Exception( $upload->getError() ); }
            }
            
            $info = $upload->upload();
            if(!$info) { throw new \Exception( $upload->getError() ); }

            foreach($info as $file){
                $json['key'] = $file['key']; //附件上传的表单名称
                $json['savepath'] = $file['savepath']; //上传文件的保存路径
                $json['name'] = $file['name']; //上传文件的原始名称
                $json['savename'] = $file['savename']; //上传文件的保存名称
                $json['picAddress'] = '/'.$upload->rootPath.$file['savepath'].$file['savename']; //上传文件的完整路径
                $json['url'] = '/'.$upload->rootPath.$file['savepath'].$file['savename']; //*针对文本控件KindEditor图片上次的数据组装
                $json['error'] = 0; //*针对文本控件KindEditor图片上次的数据组装
                $tmpAddress = 'attachment/pic/'.$file['savepath'].$file['savename'];
            }

            // // 图片宽高验证
            // $image = new \Think\Image();
            // $image->open($tmpAddress);
            // $width = $image->width(); // 返回图片的宽度
            // if($width > 200){
            //     unlink($tmpAddress);
            //     throw new \Exception( '图片过宽！' );
            // }
            // $height = $image->height(); // 返回图片的高度
            // if($height > 200){
            //     unlink($tmpAddress);
            //     throw new \Exception( '图片过高！' );
            // }

            // // 写入数据库的
            // $pic = M('pic');
            // $data['picAddress'] = $json['picAddress'];
            // $data['createTime'] = date("Y-m-d H:i:s");
            // $data['updateTime'] = date("Y-m-d H:i:s");
            // $data['width'] = $width;
            // $data['height'] = $height;
            // $pic->add($data);

            $json['if'] = $upload->rootPath;
            $json['info'] = 'success';
            $this->ajaxReturn($json,'JSONHTML');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $json['message'] = $e->getMessage(); //*针对文本控件KindEditor图片上次的数据组装
            $this->ajaxReturn($json,'JSONHTML');
        }
    }

}
