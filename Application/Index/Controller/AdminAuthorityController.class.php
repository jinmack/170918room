<?php
namespace Index\Controller;
use Think\Controller;
class AdminAuthorityController extends BaseController {

    public function index(){

    }

    // 权限页面显示
    public function adminAuthority(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminAuthority/query');
            $this->display('admin/adminAuthority/adminAuthority');
            $this->display('admin/adminAuthority/add');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 权限数据查询
    public function adminAuthorityJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $roleName = I('post.roleName',null);
            
            if(!empty($roleName)){
                $roleName = " and roleName LIKE '%".$roleName."%' ";
            }
            $whereStr = $roleName;

            $adminAuthority = M('admin_authority');
            $list = $adminAuthority->where("status != -100 $whereStr")->order('createTime desc,id')->limit(($page-1)*$rows,$rows)->select();
            $count = $adminAuthority->where("status != -100 $whereStr")->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 权限数据添加
    public function adminAuthorityAdd(){

        try {

            $roleName = I('post.roleName',null);

            // 权限列表
            $adminUser = I('post.adminUser',null);
            $authority = I('post.adminAuthority',null);

            if(empty($roleName)) { throw new \Exception( '请输入角色姓名！' ); }

            // 权限列表数据组装
            $authority = array(
                'adminUser' => $adminUser,
                'authority' => $authority,
                );
            $authority_string = json_encode($authority);

            $adminAuthority = M('admin_authority');
            $data['roleName'] = $roleName;
            $data['authority'] = $authority_string;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $adminAuthority->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工状态修改
    public function adminAuthoritySaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $adminAuthority = M('admin_authority');
            $data['status'] = $num;
            $adminAuthority->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工id查询
    public function adminAuthorityIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $adminAuthority = M('admin_authority');
            $list = $adminAuthority->where("id=$id")->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 员工数据修改
    public function adminUserSaveData(){

        try {

            $roleName = I('post.roleName',null);
            $id = I('post.id',null);

            // 权限列表
            $adminUser = I('post.adminUser',null);
            $authority = I('post.adminAuthority',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($roleName)) { throw new \Exception( '请输入角色姓名！' ); }

            // 权限列表数据组装
            $authority = array(
                'adminUser' => $adminUser,
                'authority' => $authority,
                );
            $authority_string = json_encode($authority);

            $adminAuthority = M('admin_authority');
            $data['roleName'] = $roleName;
            $data['authority'] = $authority_string;
            $adminAuthority->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 权限列表
    public function adminAuthorityList(){

        try {

            $adminAuthority = M('admin_authority');
            $list = $adminAuthority->where("status != -100")->order('createTime desc,id')->select();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }
}
