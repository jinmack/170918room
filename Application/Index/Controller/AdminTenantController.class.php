<?php
namespace Index\Controller;
use Think\Controller;
class AdminTenantController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminTenant(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminTenant/query');
            $this->display('admin/adminTenant/adminTenant');
            $this->display('admin/adminTenant/add');
            $this->display('admin/common/tail');

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
