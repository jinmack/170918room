<?php
namespace Index\Controller;
use Think\Controller;
class DaoOwnerFeeStandardController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoOwnerFeeStandard(){
    }

    public function ownerFeeStandard(){
    }

    // 数据查询
    public function ownerFeeStandardJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $phone = I('post.phone',null);
            $cellName = I('post.cellName',null);
            $buildingName = I('post.buildingName',null);
            $roomNumber = I('post.roomNumber',null);
            
            if(!empty($phone)){
                $phone = " and u.phone LIKE '%".$phone."%' ";
            }
            if(!empty($cellName)){
                $cellName = " and s.cellName LIKE '%".$cellName."%' ";
            }
            if(!empty($buildingName)){
                $buildingName = " and b.buildingName LIKE '%".$buildingName."%' ";
            }
            if(!empty($roomNumber)){
                $roomNumber = " and h.roomNumber LIKE '%".$roomNumber."%' ";
            }
            $whereStr = $phone . $cellName . $buildingName . $roomNumber;

            $list = M("ownerFeeStandard as of")
            ->join(" owner as o on of.`ownerID` = o.id ")
            ->join(" users as u on o.`userID` = u.id ")
            ->join(" house as h on of.`houseID` = h.id ")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" of.status != -100 AND o.status != -100 AND u.status != -100 AND h.status != -100 AND b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                of.id,of.chargeForWater,of.electricityFees,of.managementExpense,of.repairFee,of.networkFee,of.garbageFee,of.gasDeposit,of.liquidatedDamages,of.deposit,of.createTime,
                u.phone,u.fullName,
                h.roomNumber,h.rent,h.face,
                b.buildingName,
                s.cellName
                ")
            ->order(" of.createTime desc,of.id ")
            ->limit(($page-1)*$rows,$rows)
            ->select();

            $count = M("ownerFeeStandard as of")
            ->join(" owner as o on of.`ownerID` = o.id ")
            ->join(" users as u on o.`userID` = u.id ")
            ->join(" house as h on of.`houseID` = h.id ")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" of.status != -100 AND o.status != -100 AND u.status != -100 AND h.status != -100 AND b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                of.id
                ")
            ->order(" of.createTime desc,of.id ")
            ->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据添加
    public function ownerFeeStandardAdd(){

        try {

            $chargeForWater = I('post.chargeForWater',null);
            $electricityFees = I('post.electricityFees',null);
            $managementExpense = I('post.managementExpense',null);
            $repairFee = I('post.repairFee',null);
            $networkFee = I('post.networkFee',null);
            $garbageFee = I('post.garbageFee',null);
            $gasDeposit = I('post.gasDeposit',null);
            $liquidatedDamages = I('post.liquidatedDamages',null);
            $deposit = I('post.deposit',null);
            $roomNumber = I('post.roomNumber',null);
            $houseID = I('post.houseID',null);
            $ownerID = I('post.ownerID',null);

            if(empty($houseID)) throw new \Exception( '请输入小区/栋！' );
            if(empty($ownerID)) throw new \Exception( '请输入业主！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );

            $roomNumberIf = M("owner_fee_standard as ofs")
            ->join(" house as h on ofs.`houseID` = h.id ")
            ->where(" ofs.status != -100 AND h.status != -100 AND h.`roomNumber` = '$roomNumber' ")
            ->field("
                ofs.id,
                h.roomNumber
                ")
            ->find();
            if($roomNumberIf['roomNumber']){
                throw new \Exception( '已经添加房产，请选择其他房产！' );
            }

            $ownerFeeStandard = M('owner_fee_standard');
            $data['chargeForWater'] = $chargeForWater;
            $data['electricityFees'] = $electricityFees;
            $data['managementExpense'] = $managementExpense;
            $data['repairFee'] = $repairFee;
            $data['networkFee'] = $networkFee;
            $data['garbageFee'] = $garbageFee;
            $data['gasDeposit'] = $gasDeposit;
            $data['liquidatedDamages'] = $liquidatedDamages;
            $data['deposit'] = $deposit;
            $data['houseID'] = $houseID;
            $data['ownerID'] = $ownerID;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $ownerFeeStandard->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function ownerFeeStandardSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $ownerFeeStandard = M('owner_fee_standard');
            $data['status'] = $num;
            $ownerFeeStandard->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // ID查询
    public function ownerFeeStandardIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $list = M("owner_fee_standard as of")
            ->join(" owner as o on of.`ownerID` = o.id ")
            ->join(" users as u on o.`userID` = u.id ")
            ->join(" house as h on of.`houseID` = h.id ")
            ->join(" building as b on h.`buildingID` = b.id ")
            ->join(" small_area as s on b.`smallAreaID` = s.id ")
            ->where(" of.status != -100 AND o.status != -100 AND u.status != -100 AND h.status != -100 AND b.status != -100 AND s.status != -100 AND of.id = '$id' $whereStr ")
            ->field("
                of.id,of.chargeForWater,of.electricityFees,of.managementExpense,of.repairFee,of.networkFee,of.garbageFee,of.gasDeposit,of.liquidatedDamages,of.deposit,of.createTime,of.houseID,of.ownerID,
                u.phone,
                h.roomNumber,h.rent,h.face,
                b.buildingName,
                s.cellName
                ")
            ->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据修改
    public function ownerFeeStandardSaveData(){

        try {

            $chargeForWater = I('post.chargeForWater',null);
            $electricityFees = I('post.electricityFees',null);
            $managementExpense = I('post.managementExpense',null);
            $repairFee = I('post.repairFee',null);
            $networkFee = I('post.networkFee',null);
            $garbageFee = I('post.garbageFee',null);
            $gasDeposit = I('post.gasDeposit',null);
            $liquidatedDamages = I('post.liquidatedDamages',null);
            $deposit = I('post.deposit',null);
            $roomNumber = I('post.roomNumber',null);
            $houseID = I('post.houseID',null);
            $ownerID = I('post.ownerID',null);
            $id = I('post.id',null);

            if(empty($houseID)) throw new \Exception( '请输入小区/栋！' );
            if(empty($ownerID)) throw new \Exception( '请输入业主！' );
            // if(empty($password)) throw new \Exception( '请输入密码！' );
            // if(empty($email)) throw new \Exception( '请输入邮箱！' );
            // if(empty($phone)) throw new \Exception( '请输入电话！' );
            // if($adminAuthorityId == null) throw new \Exception( '请输入角色！' );

            // $phoneIf = M('users');
            // $phoneIf = $phoneIf->where("id=$id AND status != -100")->find();
            // if( $phone != $phoneIf['phone'] ){ // 手机发生改变时执行
            //     $phoneIf = M('users');
            //     $phoneIf = $phoneIf->where("phone=$phone AND status != -100")->find();
            //     if( $phone == $phoneIf['phone'] ){
            //         throw new \Exception( '请输入其他手机号！' );
            //     }
            // }

            // $buildingIf = M('building'); // 171021. 判断重复值，选择的方案，无法关系两张表问题，最后只能选择关系一张表问题
            // $buildingIf = $buildingIf->where("id=$id AND status != -100")->find();

            // $roomNumberIf = M("owner_fee_standard")
            // ->where(" id=$id AND status != -100 ")
            // ->find();

            // $roomNumberIf = M("owner_fee_standard as ofs")
            // ->join(" house as h on ofs.`houseID` = h.id ")
            // ->where(" ofs.status != -100 AND h.status != -100 AND h.`roomNumber` = '$roomNumber' ")
            // ->field("
            //     ofs.id,
            //     h.roomNumber
            //     ")
            // ->find();
            // if( $roomNumber != $roomNumberIf['roomNumber'] ){ // 栋名称发生改变时执行
            //     throw new \Exception( '请填入其他栋名称！' );
            //     $buildingIf = M('building');
            //     $buildingIf = $buildingIf->where("smallAreaID=$smallAreaID AND buildingName='$buildingName' AND status != -100")->find();
            //     if( $buildingName == $buildingIf['buildingName'] ){
            //         throw new \Exception( '请填入其他栋名称！' );
            //     }
            // }

            $ownerFeeStandard = M('owner_fee_standard');
            $data['chargeForWater'] = $chargeForWater;
            $data['electricityFees'] = $electricityFees;
            $data['managementExpense'] = $managementExpense;
            $data['repairFee'] = $repairFee;
            $data['networkFee'] = $networkFee;
            $data['garbageFee'] = $garbageFee;
            $data['gasDeposit'] = $gasDeposit;
            $data['liquidatedDamages'] = $liquidatedDamages;
            $data['deposit'] = $deposit;
            $data['houseID'] = $houseID;
            $data['ownerID'] = $ownerID;
            $ownerFeeStandard->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
