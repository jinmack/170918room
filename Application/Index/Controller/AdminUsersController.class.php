<?php
namespace Index\Controller;
use Think\Controller;
class AdminUsersController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminUsers(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminUsers/query');
            $this->display('admin/adminUsers/adminUsers');
            $this->display('admin/adminUsers/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 用于添加业主弹框选择用
    public function adminUsersAddOwners(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminUsersAddOwners/query');
            $this->display('admin/adminUsersAddOwners/adminUsersAddOwners');
            $this->display('admin/adminUsersAddOwners/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 用于添加租户弹框选择用
    public function adminUsersAddTenant(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminUsersAddTenant/query');
            $this->display('admin/adminUsersAddTenant/adminUsersAddTenant');
            $this->display('admin/adminUsersAddTenant/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 用户添加商户弹框选择
    public function adminUsersAddMerchant(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminUsersAddMerchant/query');
            $this->display('admin/adminUsersAddMerchant/adminUsersAddMerchant');
            $this->display('admin/adminUsersAddMerchant/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
