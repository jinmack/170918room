<?php
namespace Index\Controller;
use Think\Controller;
class DaoTenantController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoTenant(){

    }

    public function tenant(){

    }

    // 数据查询
    public function tenantJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $phone = I('post.phone',null);
            
            if(!empty($phone)){
                $phone = " and u.phone LIKE '%".$phone."%' ";
            }
            $whereStr = $phone;

            $users = M('users')->table("users as u")->join("tenant as t")->field("u.phone,u.accounts,u.nickname,u.fullName,u.headPortrait,u.eMail,t.id,t.createTime");
            $list = $users->where("u.status != -100 AND t.status != -100 $whereStr AND u.`id` = t.`userID` ")->order('t.createTime desc,t.id')->limit(($page-1)*$rows,$rows)->select();
            $users = M('users')->table("users as u")->join("tenant as t")->field("u.id");
            $count = $users->where("u.status != -100 AND t.status != -100 $whereStr AND u.`id` = t.`userID` ")->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function tenantSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $tenant = M('tenant');
            $data['status'] = $num;
            $tenant->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 添加用户到租户里
    public function usersAddTenant(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            $tenantIf = M('tenant');
            $tenantIf = $tenantIf->where("userID=$id AND status != -100")->find();
            if( $id == $tenantIf['userID'] ){
                throw new \Exception( '请输入其他业主！' );
            }

            $tenant = M('tenant');
            $data['userID'] = $id;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $tenant->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
