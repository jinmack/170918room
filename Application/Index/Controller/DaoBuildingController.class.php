<?php
namespace Index\Controller;
use Think\Controller;
class DaoBuildingController extends BaseController {

    public function index(){

        // dump($_GET);
        // dump($_POST);
        // dump($_COOKIE);
        // dump($_SESSION);
        // dump($_SERVER["REMOTE_ADDR"]);
        // $xxx->getLastSql();  
    }

    public function daoBuilding(){

    }

    public function building(){

    }

    // 数据查询
    public function buildingJsonSeleft(){

        try {

            $page = I('post.page',1);
            $rows = I('post.rows',10);
            $cellName = I('post.cellName',null);
            $buildingNo = I('post.buildingNo',null);
            $buildingName = I('post.buildingName',null);

            if(!empty($cellName)){
                $cellName = " and s.cellName LIKE '%".$cellName."%' ";
            }
            if(!empty($buildingNo)){
                $buildingNo = " and b.buildingNo LIKE '%".$buildingNo."%' ";
            }
            if(!empty($buildingName)){
                $buildingName = " and b.buildingName LIKE '%".$buildingName."%' ";
            }
            $whereStr = $cellName . $buildingNo . $buildingName;

            $list = M("building as b")
            ->join(" small_area as s on b.`smallAreaID` = s.`id` ")
            ->where(" b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                b.id,b.buildingNo,b.buildingName,b.buildingFloor,b.structure,b.supportingFacilities,b.describe,b.picture,b.createTime,
                s.cellName
                ")
            ->order(" b.createTime desc,b.id ")
            ->limit(($page-1)*$rows,$rows)
            ->select();

            $count = M("building as b")
            ->join(" small_area as s on b.`smallAreaID` = s.`id` ")
            ->where(" b.status != -100 AND s.status != -100 $whereStr ")
            ->field("
                b.id
                ")
            ->order(" b.createTime desc,b.id ")
            ->count();

            $json['info'] = 'success';
            $json['total'] = $count;
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据添加
    public function buildingAdd(){

        try {

            $smallAreaID = I('post.smallAreaID',null);
            $buildingName = I('post.buildingName',null);
            $buildingFloor = I('post.buildingFloor',null);
            $structure = I('post.structure',null);
            $supportingFacilities = I('post.supportingFacilities',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);

            if(empty($smallAreaID)) { throw new \Exception( '请输入小区！' ); }
            if(empty($buildingName)) { throw new \Exception( '请输入栋名称！' ); }

            $buildingNameIf = M('building')->table("building as b")->join("small_area as s")->field("s.id,s.cellName,b.buildingName");
            $buildingNameIf = $buildingNameIf->where(" s.id=$smallAreaID AND b.buildingName='$buildingName' AND b.status != -100 AND s.status != -100 AND b.`smallAreaID` = s.`id` ")->find();
            if( $smallAreaID == $buildingNameIf['id'] ){
                throw new \Exception( '请填入其他栋名称！' );
            }

            $building = M('building');
            $buildingNo = 'bu_' . date("ymdHis") . 'dn' . rand(11111, 99999);
            $data['buildingNo'] = $buildingNo;
            $data['buildingName'] = $buildingName;
            $data['buildingFloor'] = $buildingFloor;
            $data['structure'] = $structure;
            $data['supportingFacilities'] = htmlspecialchars_decode($supportingFacilities);
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);
            $data['smallAreaID'] = $smallAreaID;
            $data['status'] = 0;
            $data['createTime'] = date("Y-m-d H:i:s");
            $building->add($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 状态修改
    public function buildingSaveStatus(){

        try {

            $id = I('post.id',null);
            $num = I('post.num',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }
            if(empty($num)) { throw new \Exception( '数据错误！' ); }

            $building = M('building');
            $data['status'] = $num;
            $building->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // ID查询
    public function buildingIdSelect(){

        try {

            $id = I('post.id',null);

            if(empty($id)) { throw new \Exception( '数据错误！' ); }

            // $building = M('building');
            // $list = $building->where("id=$id")->find();

            $building = M('building')->table("building as b")->join("small_area as s")->field("
                b.id,b.buildingName,b.buildingFloor,b.structure,b.supportingFacilities,b.describe,b.picture,b.smallAreaID,
                s.cellName
                ");
            $list = $building->where("b.id=$id")->find();

            $json['info'] = 'success';
            $json['rows'] = $list;
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

    // 数据修改
    public function buildingSaveData(){

        try {

            $smallAreaID = I('post.smallAreaID',null);
            $buildingName = I('post.buildingName',null);
            $buildingFloor = I('post.buildingFloor',null);
            $structure = I('post.structure',null);
            $supportingFacilities = I('post.supportingFacilities',null);
            $describe = I('post.describe',null);
            $picture = I('post.picture',null);
            $id = I('post.id',null);

            if(empty($smallAreaID)) { throw new \Exception( '请输入小区！' ); }
            if(empty($buildingName)) { throw new \Exception( '请输入栋名称！' ); }

            $buildingIf = M('building'); // 171021. 判断重复值，选择的方案，无法关系两张表问题，最后只能选择关系一张表问题
            $buildingIf = $buildingIf->where("id=$id AND status != -100")->find();
            if( $buildingName != $buildingIf['buildingName'] ){ // 栋名称发生改变时执行
                $buildingIf = M('building');
                $buildingIf = $buildingIf->where("smallAreaID=$smallAreaID AND buildingName='$buildingName' AND status != -100")->find();
                if( $buildingName == $buildingIf['buildingName'] ){
                    throw new \Exception( '请填入其他栋名称！' );
                }
            }

            $building = M('building');
            $data['buildingName'] = $buildingName;
            $data['buildingFloor'] = $buildingFloor;
            $data['structure'] = $structure;
            $data['supportingFacilities'] = htmlspecialchars_decode($supportingFacilities);
            $data['describe'] = htmlspecialchars_decode($describe);
            $data['picture'] = htmlspecialchars_decode($picture);
            $data['smallAreaID'] = $smallAreaID;
            $building->where("id=$id")->save($data);

            $json['info'] = 'success';
            $this->ajaxReturn($json,'json');

        } catch (\Exception $e) {
            $json['info'] = $e->getMessage();
            $this->ajaxReturn($json,'json');
        }
    }

}
