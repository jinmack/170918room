<?php
namespace Index\Controller;
use Think\Controller;
class AdminBuildingController extends BaseController {

    public function index(){

    }

    // 页面显示
    public function adminBuilding(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminBuilding/query');
            $this->display('admin/adminBuilding/adminBuilding');
            $this->display('admin/adminBuilding/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    // 栋添加小区
    public function adminBuildingAddSmallArea(){

        try {

            $this->display('admin/common/head');
            $this->display('admin/adminBuildingAddSmallArea/query');
            $this->display('admin/adminBuildingAddSmallArea/adminBuildingAddSmallArea');
            $this->display('admin/adminBuildingAddSmallArea/add');
            $this->display('admin/common/tail');
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
