<?php
return array(

	// 线下配置-测试
	//'配置项'=>'配置值'
	'DB_TYPE' => 'mysql', // 数据库类型
	'DB_HOST' => '127.0.0.1', // 服务器地址
	'DB_NAME' => '170918room', // 数据库名
	'DB_USER' => 'root', // 用户名
	'DB_PWD' => 'root', // 密码
	'DB_PORT' => 3306, // 端口
	'DB_PREFIX' => '', // 数据库表前缀
	'DB_CHARSET'=> 'utf8', // 字符集
	'DB_PARAMS' => array(\PDO::ATTR_CASE => \PDO::CASE_NATURAL), // 修正数据库字段全部小写问题

	'session_auto_start' => true, //开启session

	'TMPL_L_DELIM'          =>  '{', // 模板引擎普通标签开始标记
	'TMPL_R_DELIM'          =>  '}', // 模板引擎普通标签结束标记

    'DEFAULT_MODULE'        =>  'index',  // 默认模块
    'DEFAULT_CONTROLLER'    =>  'distribute', // 默认控制器名称
    'DEFAULT_ACTION'        =>  'index', // 默认操作名称







);
